---
title: Outils sympathiques de Chrome DevTools
author: Nathan Rocher
tags: ["Chrome", "Javascript", "DevTools"]
summary: A REMPLIR /!\ A REMPLIR /!\ A REMPLIR /!\ A REMPLIR /!\ A REMPLIR /!\ A REMPLIR /!\ A REMPLIR /!\ 
date: 2019-09-27 13:45:00
---


![Logo de Chrome Devtools](chrome-devtools4.png)

### Introduction
>Chrome DevTools est un ensemble d'outils de développement Web intégrés directement dans le navigateur Google Chrome.
>DevTools peut vous aider à éditer des pages à la volée et à diagnostiquer rapidement les problèmes, ce qui vous aide en fin de compte à créer de meilleurs sites Web, plus rapidement.

Traduction de la présentation de [Chrome DevTools](https://developers.google.com/web/tools/chrome-devtools).

### 1. Épingler des variables à la console
![Épingler des variables à la console](epingler-variables.gif)

### 2. Capture d'écran
![Capture d'écran](capture-ecran.gif)