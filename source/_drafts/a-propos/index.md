---
title: Qui suis-je ?
date: 2019-09-11 22:23:39
---

![Nantes](nantes.jpg)

Je m’appelle Nathan Rocher, j’ai 19 ans.
Je suis passionné par l’informatique et les algorithmes d'optimisations (pour ne pas dire IA).
J'aime aussi la photographie que je pratique dans mon temps libre.
J'aime discuter dans différentes langues avec des personnes du monde entier.

### Mes études
J’ai réalisé un Bac STI2D option *Système d'Information Numérique* et je suis actuellement en DUT Informatique à Nantes.
Je fais partie du parcours PEIP de Polytech.
J'ai également obtenu le **Statut National Étudiant Entrepreneur** avec mon projet [Fiplo](#Fiplo).

### Mes projets 
Depuis tout petit, je réalise des projets. Cela me permet d'apprendre par moi-même et de découvrir bien plus que ce que l'on apprend en cours. Ci-dessous quelques un de mes projets que j'ai réalisé.

### [Conduite autonome](https://gitlab.com/3007nat/conduite-autonome)
Pour mon projet du bac STI2D, mon groupe et moi avons décidé d'améliorer les voitures actuelles. Personnellement, j'ai décidé de réaliser un système permettant de recentrer automatiquement une voiture dans sa voie de circulation. Le but de ce projet est de réduire le nombre d'accidents causé par la fatigue au volant. Cependant ce projet aurait pu servir pour conduire une voiture automatiquement sur des axes routiers bien entretenus.
<details>
<summary>En savoir plus sur ce projet</summary>

Pour réaliser ce projet, j’ai fixé une Raspberry PI avec une caméra sur le tableau de bord d'une voiture. La caméra filmant la route m'a ensuite permis de faire de l’analyse d’image. J'ai réalisé le programme en Python avec la librairie OpenCV. Malgré les nombreuses optimisations, le programme reste lent sur Raspberry PI mais fonctionne correctement avec un I7.
Ce projet est open-source, vous pouvez le voir sur [ce repo](https://gitlab.com/3007nat/conduite-autonome) de mon Gitlab.

Rendu du programme :
![Conduite Autonome](conduite-autonome.gif)
</details>

### Fiplo
Depuis le lycée, je développe un système composé de serveur et de client web/mobile/desktop permettant de partager des fichiers. Ce site qui, au début, était accessible par tout le monde permettait de créer un dossier en ligne pour se partager des fichiers pour une courte durée.
Maintenant, il s'agit d'un système permettant de synchroniser ses fichiers à travers tous types d'appareils en temps réel. Avec le temps j'ai compris qu'il était important de sécuriser ses données. C'est pourquoi Fiplo utilise une encryption Client-to-Client, c'est-à-dire que les fichiers sont encryptés dans le client, stocké crypté, et uniquement décrypté par le client quand il le souhaite.