---
title: Recentrer une voiture dans sa voie de circulation
author: Nathan Rocher
tags: ["Analyse d'image", "Vehicules", "Python", "OpenCV"]
summary: Utilisons Python et OpenCV pour découvrir comment recentrer une voiture dans sa voie de circulation.
date: 2019-12-01 11:33:00
---

### Introduction
Comment réduire le nombre d’accidents sur les routes, réduire la pollution due aux transports ?
C'est la question à laquelle j'ai dû répondre pour le projet de mon BAC STI2D.
Compliqué ? Oui. J'aime les défis. 🤟

### Contexte
Le nombre d'accidents mortels sur les routes était en baisse depuis 2010 cependant une hausse de 2013 à 2015  vient perturber l'objectif de la sécurité routière. Cependant, depuis 2016, ce nombre tend à rejoindre l'objectif de -30%. Une grande partie de ces accidents se déroule lors des horaires de pointes sur les routes hors agglomérations et sur les autoroutes. 27% de ces accidents sont directement liés à la vitesse et 18% à l'alcool.

[📜 Lire le « Bilan 2018 de la sécurité routière »](https://www.onisr.securite-routiere.interieur.gouv.fr/etat-de-l-insecurite-routiere/bilans-annuels-de-la-securite-routiere/bilan-2018-de-la-securite-routiere)

### Solution
Une solution **envisageable** pour résoudre ce problème est *simplement* d'enlever le volant des mains des humains.
**Dans le cas où** une grande partie de notre société accepterait de changer de moyen de transport comme les voitures autonomes ou l'utilisation d'un moyen de transport alternatif, certaines personnes utiliseraient toujours ce moyen de transport par manque de moyens / envie / passion. 🤷‍♂️
Il n'existe pas de solution *parfaite*, car un accident est parfois inévitable.

### Retirer petit à petit les mains du volant
De plus en plus de véhicules sont équipés d’une nouvelle technique que les constructeurs de véhicules appellent « Lane assist ». Pour les premières versions de ce système, un voyant s’allumait si la voiture pense que le conducteur est sorti de sa voie de circulation. Aujourd’hui, les systèmes, bien plus complexe, permettent d’agir sur la direction de la voiture afin de la recentrer automatiquement dès que la route est correctement délimitée.

Couplé à d'autres assistants, l'humain ne contrôle que le bon déroulement des algorithmes, car celui-ci peut reprendre la main dès qu'il le souhaite.

Pour bien comprendre l'algorithme que nous allons développer par la suite, je vous propose de regarder une courte vidéo mise en ligne par « Volkswagen France » qui explique le concept.
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/AD_m1dKxHPI?disablekb=1&loop=1&modestbranding=1&iv_load_policy=3" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Comment allons-nous résoudre ce problème ?
Pour résoudre ce problème, nous allons utiliser Python, NumPy, et OpenCV. 
1. Acquisition des images / vidéos
	- Lors de l'acquisition des images ou des vidéos, il est important à ce que la caméra soit correctement centrée au milieu du tableau de bord et que celle-ci ne se déplace pas.
	- Nettoyer correctement le pare-brise
	- Si la caméra que vous utilisez à un grand-angle alors il faudra supprimer cet effet enfin d'avoir une image à plat.
2. Nettoyage des données
	- Garder des séquences de 10 à 50 secondes
	- Garder en priorité les séquences avec des virages, et des ombres.
	- Si une voiture devant vous est trop proche alors il se peut que le programme ne puisse pas détecter les lignes.  
3. Analyse d'image
	L'algorithme va fonctionner de la manière suivante :
	1. « R.O.I » --> Découpage de l'image pour obtenir une zone où les lignes blanches devraient être.
	2. « Bird View » --> Transformation des perspectives afin de récupérer une vue de dessus.
	3. Obtention des lignes blanches avec utilisation de masque
		4. Optimisation en utilisation les valeurs précédentes
	4. Régression polynomiale pour chacune des lignes blanches
	5. Analyse des résultats en Y = 0
		6. Stockage de la valeur courante
	6. Affichage du résultat au conducteur

### Acquisition des données
La première solution est d'utiliser une dashcam ; il s'agit d'une petite caméra qui se fixe à l'intérieur de l'habitacle contre le pare-brise avec une ventouse. Elles sont pratiques mais la qualité d'image n'est généralement pas de très bonne qualité.
Une autre solution est d'utiliser une caméra du style `GoPro` et la fixer avec une ventouse sur le pare-brise. *(Méthode que j'ai utilisée.)*

### Nettoyer les données
Pour nettoyer les données filmées par notre caméra, nous allons effectuer des tris afin de choisir dans un premier temps des séquences qui ont l'air faciles à analyser (ligne droite, virage léger, plein soleil, visibilité parfaite, chaussée refaite) puis dans un second temps des séquences plus complexes (changement de voie, virage fort, sortie de tunnel, contre-jour, pluie, chaussée déformée). Des séquences, de 10 à 50 secondes, sans saletés sur le pare-brise feront l'affaire. De nos jours, les caméras s'adaptent automatiquement à la lumière et elles font une très bonne balance des blancs, cependant il se peut que votre caméra ait choisi un mauvais réglage. GARDER, les séquences ! En effet, ces séquences sont donc plus difficiles à analyser. On peut donc les tester sur notre modèle afin de le tester sur un maximum de condition.

### Analyser les données
Créons maintenant l'algorithme qui va nous permettre de recentrer automatiquement un véhicule sur la route.
Nous allons commencer par les différentes fonctions qui seront utiles pour analyser l'image, puis nous verrons comment analyser, image par image, la route.

#### Importer les librairies
Pour réaliser cet algorithme, nous allons utiliser les librairies suivantes : `OpenCV`, `Numpy`, `Matplotlib`, `Python Imaging Library (PIL)`, `SciPy`, et `scikit-image`.
Commençons donc par les importer.

```PYTHON
import os
import time 

import cv2 as cv
import numpy as np
from math import *

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from scipy import ndimage
from skimage import exposure
from PIL import ImageEnhance, Image, ImageOps, ImageFilter
```

S'il vous manque certaines librairies, vous pouvez les installer via `pip install [nom librairie]`.

#### Définissons les fonctions qui vont servir à l'analyse
Je vais décrire chacune des fonctions qui permettent l'analyse d'image.
Par la suite, nous écrirons le `main` du programme qui appellera ces fonctions. 

#### 1. Region of interest
Cette méthode permet de récupérer la zone de l'image que l'on souhaite.
Plus tard, lors de l'analyse image par image de la vidéo, nous allons découper l'image en entrée afin de délimiter où la route est supposée être.

```PYTHON
def RegionOfInterest(img, points):
    mask = np.zeros_like(img)
    if len(img.shape) > 2:
        channel_count = img.shape[2]
        ignore_mask_color = (255,) * channel_count
    else:
        ignore_mask_color = 255
    cv.fillPoly(mask, [points], ignore_mask_color)
    masked_image = cv.bitwise_and(img, mask)
    return masked_image
```

#### 2. BirdView
À partir de la R.O.I, nous allons créer une vue "d'oiseau". C'est-à-dire que c'est comme si nous allons déplacer la caméra à 90° au-dessus de la route.
En réalité, nous allons en quelque sorte écraser le trapèze du R.O.I.

```PYTHON
def CalculeDesMatricesDePerspective(src, dst):
    M = cv.getPerspectiveTransform(src, dst)
    M_inv = cv.getPerspectiveTransform(dst, src)
    return M, M_inv

def AppliqueLesMatrices(img, M):
    image_size = (img.shape[1], img.shape[0])
    BirdView = cv.warpPerspective(img, M, image_size, flags=cv.INTER_LINEAR)
    return BirdView
```

#### 3. Obtention des lignes blanches
Cette fonction prend en entrée une image, et un seuil de couleur.
Elle retourne les couleurs entre la valeur d'entrée et la valeur maximum (Ici le blanc (255, 255, 255)).
Cette fonction est appliquée après une amélioration de l'image.

```PYTHON
def ObtientLesBlancs(image, color): 
  lower = np.uint8([color, color, color])
  upper = np.uint8([255, 255, 255])
  white_mask = cv.inRange(image, lower, upper)
  return white_mask
```

Afin de convertir les lignes blanches en point, nous allons réaliser des fenêtres glissante qui vont glisser sur 15 points (hauteur de l'image divisée par 15). Ce nombre de points est largement suffisant pour avoir une représentation de la courbe de la ligne blanche. Par la suite, on va faire une moyenne d'où se trouve la zone blanche.
En cas de valeur non trouvé, la fonction retournera les valeurs précédentes.

```PYTHON
def cluster(data, maxgap):
  data.sort()
  groups = [[data[0]]]
  for x in data[1:]:
      if abs(x - groups[-1][-1]) <= maxgap:
          groups[-1].append(x)
      else:
          groups.append([x])
  return groups

def PointsCentralsDesLignesBlanches(hist):
  ClusteredData = cluster(hist, 100)
  PointX = []
  for data in ClusteredData:
    PointX.append(int(np.mean(data)))
  return PointX

def FenetreGlissante(image, valeurPrecedente):
  height, width = image.shape
  x = width - 1
  HeightStopNumber = 15
  HeightOfOneStop = ceil(height / HeightStopNumber)

  CenterImage = int(x/2)

  PgX = []
  PgY = []
  PdX = []
  PdY = []

  for HeightStop in range(0, HeightStopNumber):
    PositionY = HeightOfOneStop * HeightStop
    NextPositionY = PositionY + HeightOfOneStop
    Hist = [sum(x) for x in zip(*image[PositionY:NextPositionY, 0:x])]
    PointX = PointsCentralsDesLignesBlanches([i for i, x in enumerate(Hist) if x == max(Hist)])

    for ptn in PointX:
      if(ptn < CenterImage*0.8):
        PgX.append(ptn)
        PgY.append(round((NextPositionY+PositionY)/2))
      if(ptn > CenterImage*1.2):
        PdX.append(ptn)
        PdY.append(round((NextPositionY+PositionY)/2))

  if(len(valeurPrecedente) == 4):
    old_PgX, old_PgY, old_PdX, old_PdY = valeurPrecedente

    if(PdX == []) :
      PdX = old_PdX
      PdY = old_PdY

    if(PgX == []) :
      PgX = old_PgX
      PgY = old_PgY

  PdX = np.insert(PdX, 0, np.nanmean(PdX), axis=0)
  PdY = np.insert(PdY, 0, 10, axis=0)

  PgX = np.insert(PgX, 0, np.nanmean(PgX), axis=0)
  PgY = np.insert(PgY, 0, 10, axis=0)

  PdX = np.insert(PdX, 0, np.nanmean(PdX), axis=0)
  PdY = np.insert(PdY, 0, 450, axis=0)

  PgX = np.insert(PgX, 0, np.nanmean(PgX), axis=0)
  PgY = np.insert(PgY, 0, 450, axis=0)

  return PgX, PgY, PdX, PdY
```

#### 4. Réalisation du `main`
Créons le cœur du programme.
Nous allons donc analyser image par image, une vidéo d'une centaine d'images.

J'ajoute en plus cette fonction `fig2img` qui permettra de générer un GIF automatiquement (avec d'autres méthodes dans le main).
Elle ne fait pas partie de l'analyse d'image.

```PYTHON
def fig2img (fig):
    fig.canvas.draw ()
    w,h = fig.canvas.get_width_height()
    buf = np.fromstring (fig.canvas.tostring_argb(), dtype=np.uint8)
    buf.shape = (w, h, 4)
    buf = np.roll (buf, 3, axis = 2)
    w, h, _ = buf.shape
    return Image.frombytes("RGBA", (w, h), buf)
```

J'ajoute cette autre fonction `OnDessineLaRoute` qui permet de dessiner en vert la zone où la route devrait être.

```PYTHON
def OnDessineLaRoute(Dimensions, M_INV, frame, ploty, left_fitx, right_fitx):

    color_warp = np.zeros(Dimensions.shape).astype(np.uint8)

    pts_left = np.array([np.transpose(np.vstack([left_fitx, ploty]))])
    pts_right = np.array([np.flipud(np.transpose(np.vstack([right_fitx, ploty])))])
    pts = np.hstack((pts_left, pts_right))

    cv.fillPoly(color_warp, np.int32([pts]), (0,255, 0))

    newwarp = cv.warpPerspective(color_warp, M_INV, (Dimensions.shape[1], Dimensions.shape[0])) 

    return cv.addWeighted(frame, 1, newwarp, 0.3, 0)
```

Voici le `main` qui permet de lire image par image, analyser, et créer un GIF.

Si vous n'avez pas de vidéo pour essayer, vous pouvez vous servir de celle que j'ai filmé en les téléchargeant [ici](https://gitlab.com/nrocher/conduite-autonome/tree/master/Prise%20de%20vue).

```PYTHON
NBRIMAGE = 0
ID_VIDEO = 50
cap = cv.VideoCapture("../Prise de vue/output-" + str(ID_VIDEO) + ".avi")
GIF_FRAMES = []
ret, frame = cap.read()
fig = plt.figure(figsize = (10, 10))
valeurPrecedente = []

while(True):

  print("\n IMAGE : " + str(NBRIMAGE))
  
  ## ------------------ Def : On definit le tableau de matplotlib
  columns = 2
  rows = 2

  fig.add_subplot(rows, columns, 1)
  plt.axis('off')
  plt.title("Prise de vue")
  plt.imshow(frame)

  ## ------------------ Alg : On definit la R.O.I. de l'image
  ImageAvecRegionOfInterest = frame

  DistanceFromBottom = 75
  DistanceFromSide = 0
  HeightROI = 140
  WidthCenterROI = 175

  bottom_px = ImageAvecRegionOfInterest.shape[0] - 1
  right_px = ImageAvecRegionOfInterest.shape[1] - 1

  pts_bas_gauche = [ DistanceFromSide, bottom_px - DistanceFromBottom ]
  pts_bas_droit = [ right_px - DistanceFromSide, (bottom_px - DistanceFromBottom) ]
  pts_haut_gauche = [ (right_px-WidthCenterROI) / 2, (bottom_px - DistanceFromBottom - HeightROI) ]
  pts_haut_droit = [ (right_px+WidthCenterROI) / 2, (bottom_px - DistanceFromBottom - HeightROI) ]

  transformation_ROI = np.array([pts_bas_gauche, pts_haut_gauche, pts_haut_droit, pts_bas_droit], np.int32)

  ROI = RegionOfInterest(frame, transformation_ROI)

  ## ------------------ Alg : On applique les matrices de Perspective
  src_pts = transformation_ROI.astype(np.float32)
  dst_pts = np.array([[0, bottom_px], [0, 0], [right_px, 0], [right_px, bottom_px]], np.float32)
  M, M_inv = CalculeDesMatricesDePerspective(src_pts, dst_pts)
  BirdView = AppliqueLesMatrices(frame, M)

  ## ------------------ Alg : On transforme les couleurs et on fais un masque
  BirdViewAugmented = Image.fromarray(BirdView) # Numpy -> PIL
  BirdViewAugmented = ImageEnhance.Brightness(BirdViewAugmented).enhance(0.02)
  BirdViewAugmented = ImageOps.autocontrast(BirdViewAugmented)
  BirdViewAugmented = ImageOps.equalize(BirdViewAugmented)
  BirdViewAugmented = ImageEnhance.Contrast(BirdViewAugmented).enhance(10)

  kernel = np.ones((5, 5), np.uint8)
  BirdViewAugmented = np.array(BirdViewAugmented).astype("uint8") # PIL -> Numpy
  BirdViewAugmented = ObtientLesBlancs(BirdViewAugmented, 150)
  BirdViewAugmented = cv.dilate(BirdViewAugmented, kernel, iterations=1)

  BirdViewLigneBlanche = np.array(BirdViewAugmented, np.uint8)
 
  fig.add_subplot(rows, columns, 2)
  plt.axis('off')
  plt.title("Lignes Blanches")
  plt.imshow(BirdViewLigneBlanche)
  


  ## ------------------ Alg : On creer les points des lignes blanches et on les regroupes
  PgX, PgY, PdX, PdY = FenetreGlissante(BirdViewLigneBlanche, oldWindows)
  oldWindows = [PgX, PgY, PdX, PdY]

  ## ------------------ Regression polynomiale 
  left_fit = np.polyfit(PgY, PgX, 2)
  right_fit = np.polyfit(PdY, PdX, 2)

  lane_width_px=400
  lane_center_px_psp=300

  ImageHeight, ImageWidth = BirdViewLigneBlanche.shape
  ploty = np.linspace(0, ImageHeight-1, ImageHeight)
  y_eval = np.max(ploty)

  ## ------------------ Alg : On affiche l'image actuelle avec la zone ou est la voie de circulation
  left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
  right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]

  center_offset = (((left_fit[0]*y_eval**2 + left_fit[1]*y_eval  + left_fit[2]) + \
                    (right_fit[0]*y_eval**2 + right_fit[1]*y_eval + right_fit[2])) \
                  / 2) - lane_center_px_psp

  print("Center of the road : " + str(center_offset))

  
  ## ------------------ Alg : On affiches les regressions polynomiales
  fig.add_subplot(rows, columns, 3)
  plt.axis('off')
  plt.title("Regressions polynomiales")
  plt.imshow(BirdViewLigneBlanche)
  plt.plot(left_fitx, ploty, color='green', linewidth=5)
  plt.plot(right_fitx, ploty, color='pink', linewidth=5)


  ## ------------------ Alg : On affiche l'image actuelle avec la zone ou est la voie de circulation
  fig.add_subplot(rows, columns, 4)
  plt.axis('off')
  plt.title("Sortie Finale")
  route_avec_masque_de_la_voie = OnDessineLaRoute(BirdView, M_inv, frame, ploty, left_fitx, right_fitx)
  plt.imshow(route_avec_masque_de_la_voie)

  GIF_FRAMES.append(fig2img(fig))

  fig.clf()
  NBRIMAGE = NBRIMAGE + 1

  ## ------------------ Alg : Enregistre un gif toute les 10 images
  if NBRIMAGE % 10 == 0 :
    GIF_FRAMES[0].save('sortie_programme' + str(NBRIMAGE) + '.gif', format='GIF', append_images=GIF_FRAMES[1:], save_all=True, duration=100, loop=0)

  ## ------------------ Alg : Si il y a plus d'image on quitte la boucle
  ret, frame = cap.read()
  if np.shape(frame) == () :
    break

GIF_FRAMES[0].save('sortie_programme.gif', format='GIF', append_images=GIF_FRAMES[1:], save_all=True, duration=100, loop=0)

cap.release()
```

### Résultat
Testons ! Pour changer la vidéo analysée, il faut changer la valeur `ID_VIDEO` qui se situe au-dessus du `main`.

<details>
<summary>Résultat n°1</summary>
  <img src="1.gif" />
</details>

<details>
<summary>Résultat n°2</summary>
  <img src="2.gif" />
</details>

<details>
<summary>Résultat n°3</summary>
  <img src="3.gif" />
</details>

<details>
<summary>Résultat n°4</summary>
  <img src="4.gif" />
</details>

<details>
<summary>Résultat n°5</summary>
  <img src="5.gif" />
</details>

### Conclusion
Comme nous venons de le voir, le résultat est correct lorsque la route est bien délimité, qu'il y a une bonne météo, et qu'il n'y a pas d'ombre. Une approche basée sur un réseau de neurones artificiels pourrait obtenir un meilleur résultat afin de résoudre ce problème, car il serait capable de s'adapter à un plus grand nombre de situations.

Pour le moment, des intelligences artificielles capables de réaliser ces tâches sont à l'étude dans de nombreuses entreprises à travers le globe, mais il existera toujours des accidents inévitables ou des erreurs de programmation.

On peut alors se demander comment une intelligence artificielle pourrait prendre un choix face à cette situation :
*Que faire dans le cas où deux solutions sont possibles, mais que dans ces deux solutions une ou plusieurs personnes meurent.*
Le site [Moral Machine (MIT)](http://moralmachine.mit.edu/hl/fr) essaye d'apporter une réponse basée sur le choix des internautes.