---
title: Partitionner et formater un disque dur
author: Nathan Rocher
tags: ["SysAdmin", "Linux", "Stockage"]
summary: Nous allons voir dans ce tutoriel comment partitionner et formater correctement un nouveau périphérique de stockage. 
date: 2019-09-12 21:16:00
---
### Introduction
Nous utiliserons dans ce tutoriel `fdisk` et `lsblk`.
Ces deux programmes sont de base inclus dans le paquet standard `util-linux`.

[📖 man fdisk](https://man.cx/fdisk(8)/fr) : Ouvrir la page manuel de `fdisk`
[📖 man lsblk](https://man.cx/lsblk(8)/fr) : Ouvrir la page manuel de `lsblk`

Ce tutoriel vous montrera comment :
- Identifier les périphériques de stockage 
- Créer une partition
- Formater une partition en Ext4
- Monter une partition

### Identifier les périphériques de stockage
Avant de commencer, il est important de bien identifier le matériel présent sur la machine.
Pour ce faire, nous allons utiliser `lsblk`.
Ce programme permet d'afficher des renseignements sur les périphériques blocs disponibles.
L'option `-I 8` permet d'afficher uniquement les disques et les partitions.

```bash
$ lsblk -I 8
```
Sortie du programme :
```
NAME    MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda       8:0    0   500G  0 disk
└─sda1    8:1    0   500G  0 part /mnt/data
sdb       8:16   0   500G  0 disk
sdc       8:32   0   500G  0 disk
└─sdc1    8:33   0   500G  0 part /
```
Sur cette machine, il y a trois périphériques de stockage de branchés.
- Le disque `sda` a une partition accessible via `/mnt/data`.
- Le disque `sdb` n'a aucune partition. (Il s'agit du disque que l'on vient de rajouter)
- Le disque `sdc` a une partition qui contient le système d'exploitation de la machine.

### Partitionner le disque
Nous allons commencer par partitionner le disque en GPT (GUID Partition Table).
GPT est une manière de stocker les informations de partitionnement  sur un disque.
Cela permet à la machine de savoir ou commence une partition et ou elle finit sur le disque.
Il existe d'autres méthodes comme MBR (Master Boot Record) mais GPT est plus récente et gère mieux les disques.

>⚠️ Attention, à chaque redémarrage de la machine, les identifiants des disques peuvent changer.

Pour partitionner le disque nous allons utiliser `fdisk` sur le nouveau disque.

```bash
$ sudo fdisk /dev/sdb
```

Le programme vous demande alors qu'elle commande exécuter. Pour voir l'aide, il suffit de taper `m`.


>⚠️ Toute modification du disque peut entrainer la perte de données.
>Pensez à vérifier qu'il s'agit du bon périphérique. En tapant `p`, `fdisk` nous donne des informations sur le disque. 

Tout cela dit, partitionnons notre disque !

Pour définir une table GPT, il suffit de taper `g`. 
```
Command (m for help): g
```
Le programme devrait alors afficher ce message :
```
Created a new GPT disklabel (GUID: XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX)
```

✔️ Le disque maintenant partitionné, nous pouvons créer notre partition.

### Créer une partition
Pour créer la partition, il suffit de taper `n`.
```
Command (m for help): n
```

Le programme vous demandera quelques informations comme le numéro de partition, le premier secteur ainsi que le dernier. 

```
Partition number (1-128, default 1):
First sector (2048-1048575966, default 2048):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-1048575966, default 1048575966):
```
Appuyer simplement sur Entrée, les bonnes valeurs seront automatiquement détectées.


Le programme devrait ensuite afficher ce message :
```
Created a new partition 1 of type 'Linux filesystem' and of size 500 GiB.
```
La partition est bien créée dans la table GPT.

Maintenant, il nous reste la partie la plus importante, qui est d'appliquer au disque les modifications.
>⚠️ Si votre disque contient des données, elles seront irrécupérables après cette étape.

La commande `w` permet d'écrire sur le disque les modifications (et quitter le programme).

```
Command (m for help): w
```

Le message suivant devrait apparaître :
```
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks
```

🎉 La partition est maintenant créée. Formatons-la !

### Formater la partition avec Ext4
Pour pouvoir écrire et lire des fichiers, il faut définir un système de fichiers.
Ext4 est le système de fichiers par default de Linux.
La taille maximum d'un fichier est de 16 To ( ce qui représente un très très gros fichier texte 😄).

Commençons par regarder l'identifiant de notre partition :
```bash
$ lsblk -I 8
```
Sortie du programme :
```
NAME    MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda       8:0    0   500G  0 disk
└─sda1    8:1    0   500G  0 part /mnt/data
sdb       8:16   0   500G  0 disk
└─sdb1    8:17   0   500G  0 part <-- La partition que l'on vient de créer
sdc       8:32   0   500G  0 disk
└─sdc1    8:33   0   500G  0 part /
```

La partition que l'on vient de créer est accessible via l'identifiant `/dev/sdb1`.

Nous allons formater la partition avec un label, cela nous permettra de différencier les périphériques de stockage plus simplement avec la commande `lsblk -o NAME,LABEL,FSTYPE,SIZE,MOUNTPOINT -I 8` (nécessaire pour la suite).

J'ai décidé de choisir "yeahh" pour le Label de mon disque dur.

Pour formater la partition, lancez :
```bash
$ sudo mkfs.ext4 -L yeahh /dev/sdb1
```
[📖 man mkfs.ext4](https://man.cx/mkfs.ext4(8)/fr) : Ouvrir la page manuel de `mkfs.ext4`

Pour changer le label de la partition :
```bash
$ sudo e2label /dev/sdb1 nouveau-label
```
[📖 man e2label](https://man.cx/e2label(8)/fr) : Ouvrir la page manuel de `e2label`

🥂 La partition est créée et formatée. Il ne reste plus qu'à la monter ! 

### Monter la nouvelle partition
Généralement, les périphériques de stockage sont montés dans le répertoire `/mnt/`.
Ne dérogeons pas à la règle, et créons le dossier `/mnt/yeahh` 🤟.
Vous pouvez bien évidemment nommer ce dossier comme vous le souhaitez. 
```bash
$ sudo mkdir /mnt/yeahh
```
[📖 man mkdir](https://man.cx/mkdir(1)/fr) : Ouvrir la page manuel de `mkdir`

Pour monter la partition **temporairement** :
```bash
$ sudo mount /dev/sdb1 /mnt/yeahh
```
[📖 man mount](https://man.cx/mount(8)/fr) : Ouvrir la page manuel de `mount`

### Monter la partition au démarrage
Si vous souhaitez que la partition soit montée au démarrage de la machine,
Il faut ajouter une petite ligne dans le fichier `/etc/fstab` :

Ouvrez le avec votre éditeur préféré (moi c'est `nano` 👌) :
```bash
$ sudo nano /etc/fstab
```
[📖 man nano](https://man.cx/nano(1)/fr) : Ouvrir la page manuel de `nano`

Et ajoutez cette ligne à la fin du fichier :
```
LABEL=yeahh /mnt/yeahh ext4 defaults 0 2
```

Le label nous permet d'être sûr de monter la bonne partition, il est suivi du point de montage ainsi que du système de fichiers de la partition. Les trois derniers paramètres doivent être laissés par défaut (inutile dans notre cas).

### Vérifier que la partition soit bien montée
Afin de vérifier que la partition soit bien montée, nous allons utiliser `lsblk` :
Cette commande, certes plus longue, permet de n'afficher que les informations utiles.
```bash
$ lsblk -o NAME,LABEL,FSTYPE,SIZE,MOUNTPOINT -I 8
```
Sortie du programme :
```
NAME     FSTYPE   LABEL  SIZE MOUNTPOINT
sda                      500G
└─sda1    ext4    data   500G /mnt/data
sdb                      500G
└─sdb1    ext4    yeahh  500G /mnt/yeahh <-- La partition est bien monté
sdc                      500G  
└─sdc1    ext4           500G /
```
On voit donc que la partition est bien montée dans le répertoire que l'on a créé plutôt.

Vous pouvez aussi essayer d'écrire dessus :
```bash
$ sudo nano /mnt/yeahh/First
```
Enregistrez le fichier puis lisez le avec `cat /mnt/yeahh/First`.

>💡 Pensez à vérifier les droits des utilisateurs pour être sûr que tout le monde puisse y accéder ou au contraire le rendre accessible uniquement pour certains utilisateurs.

Vous pouvez redémarrer votre machine puis effectuer la longue commande `lsblk`. 
La partition sera montée automatiquement.

### Conclusion
Votre nouveau disque dur est maintenant partitionné, formaté, monté, et prêt à être utilisé. 💾
Il existe d'autres programmes (tels que `parted`) qui peuvent être plus complexe à utiliser afin de réaliser des actions plus complexes.
`fdisk` est un très bon choix pour réaliser ce que l'on vient de faire du fait qu'il est simple d'utilisation.