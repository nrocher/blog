---
title: Mettre à jour son IP dynamique sur Cloudflare
author: Nathan Rocher
tags: ["Nom de domaine", "Cloudflare", "SysAdmin", "Linux"]
summary: Comment résoudre le problème d'une IP dynamique avec Cloudflare et un nom de domaine ?
date: 2019-10-25 16:30:00
---

<img alt="Logo de Cloudflare" src="cloudflare.png" style="margin-top:30px" width="350px" />

### Introduction
Aujourd'hui, pour de multiples raisons nous pouvons très simplement créer un serveur web local répondant à de nombreux critères. Avec l'arrivée de la fibre dans les maisons, il devient tout à fait possible d'héberger un site WEB ou un serveur de stockage ou une interface de gestion de domotique, ...
De plus il est très simple de créer un serveur qui consomme peu, qui ne fait pas de bruit, et qui ne prend pas de place avec une Raspbery PI par exemple.
Il reste maintenant un problème, comment se souvenir d'une IP qui peut changer ? S'envoyer un mail dès qu'il y a un changement ? Pas très réaliste. Des signaux de fumée codant l'IP en binaire ? Ça peut fonctionner, mais c'est quand même compliqué. 🤔

Une solution réaliste est bien évidemment d'acquérir un nom de domaine.
Ils ne coûtent pas très cher et ils vous facilite la vie ! 

### Acheter un nom de domaine
Pour vous aider, voici une petite liste des premiers résultats des moteurs de recherche : 
- [🇫🇷 OVH](https://www.ovh.com/fr/domaines/)
- [🇫🇷 LWS](https://www.lws.fr/nom-de-domaine.php)
- [🇺🇸 Namecheap](https://www.namecheap.com/)
- [🇺🇸 Google Domains](https://domains.google/intl/fr_fr/)

### Qu'est ce que Cloudflare ?
Cloudflare est un DNS ([Domain Name System](https://fr.wikipedia.org/wiki/Domain_Name_System)) mondial. Il offre plusieurs avantages comme la mise à jour très rapide des zones DNS, mais aussi une protection DDOS, la mise en cache de ressource statique, la mise en place automatique d'un certificat [SSL](https://fr.wikipedia.org/wiki/Transport_Layer_Security) qui permet de sécuriser l'échange de vos clients entre votre site et Internet en utilisant [HTTPS](https://fr.wikipedia.org/wiki/HyperText_Transfer_Protocol_Secure) et tout un tas d'autres outils pratiques. 🚀

> Je ne suis pas sponso, ils fournissent juste des bons services gratuits. 

### Pourquoi l'utiliser ?
Premièrement, Cloudflare permet la mise à jour rapide de zone DNS. Lors d'une mise à jour via l'API ou leur interface WEB, celle-ci est immédiatement appliquée.
Deuxièmement, Cloudflare dispose d'une API permettant de changer la configuration de la zone DNS d'un domaine.

Grâce à cette API et le changement rapide de zone DNS nous allons pouvoir mettre à jour notre IP de notre domaine régulièrement et rapidement.

### Configuration de notre serveur
Ce script va être exécuté toutes les heures sur notre serveur.
Il va fonctionner de la manière suivante :
1. Récupération de notre adresse IP
2. Si elle est différente de l'ancienne IP alors on continue
3. Enregistrement dans un fichier ``IP.temp`` de la nouvelle IP 
4. Envoie d'une requête vers l'API de Cloudflare contenant la nouvelle IP

### Token Cloudflare
Premièrement, il faut trouver sur le site de Cloudflare son token. Celui-ci permet de se connecter à son compte (avec l'adresse mail) via l'API.
Comment trouver son token API :
1. Connectez-vous à Cloudflare.
2. Cliquez sur ``Mon profil`` dans le menu déroulant en cliquant sur votre avatar en haut à droite.
3. Dans la catégorie ``Clés API``, cliquez sur ``Afficher`` à côté de ``Clé API Globale``.

✔️ Vous avez votre token ? Super, passons à la suite !

### Prérequis pour Cloudflare
Via l'interface, ajouter une entrée DNS de type ``A`` qui sera l'entrée DNS que le script va modifier par la suite.
>📜 Si vous avez d'autre entrée à ajouter, n'hésitez pas. Seul l'entrée choisie par la suite sera modifier.

### Identifiant de la zone DNS
Deuxièmement, il faut donner à l'API l'identifiant de la zone DNS que nous souhaitons modifier.
Pour cela, nous allons récupérer via ``curl`` toutes les zones DNS enregistré sur le compte en format JSON.

> Pour se faciliter la vie, vous pouvez utiliser un formateur de JSON pour lire aisément. [Celui-ci](https://jsonformatter.curiousconcept.com/) par exemple.

Dans la commande suivante, il faut remplacer :
- ``VOTRE_EMAIL`` par l'email de votre compte Cloudflare.
- ``VOTRE_TOKEN`` par le token récupéré plus haut.

```bash
$ curl \
    -X GET "https://api.cloudflare.com/client/v4/zones" \
    -H 'X-Auth-Email: VOTRE_EMAIL' \
    -H 'X-Auth-Key: VOTRE_TOKEN' \
    -H 'Content-Type: application/json'
```

### Identifiant de l'entrée DNS
Troisièmement, il faut donner à l'API l'identifiant de l'entrée DNS que nous souhaitons modifier.
Pour cela, nous allons récupérer toutes les entrées de votre zone DNS et encore une fois au format JSON.

> Pour se faciliter la vie, vous pouvez utiliser un formateur de JSON pour lire aisément. [Celui-ci](https://jsonformatter.curiousconcept.com/) par exemple

Dans la commande suivante, il faut remplacer :
- ``ZONE_DNS`` (il est dans l'URL de la requête) par la zone DNS de votre nom de domaine.
- ``VOTRE_EMAIL`` par l'email de votre compte Cloudflare.
- ``VOTRE_TOKEN`` par le token récupéré plus haut.

```bash
$ curl \
    -X GET "https://api.cloudflare.com/client/v4/zones/ZONE_DNS/dns_records" \
    -H 'X-Auth-Email: VOTRE_EMAIL' \
    -H 'X-Auth-Key: VOTRE_TOKEN' \
    -H 'Content-Type: application/json'
```

### Création du script
Maintenant que nous avons toutes les informations relatives à Cloudflare, passons à la création du script qui permettra de faire la mise à jour si celle-ci est utile.
J'ai enregistré le script dans le répertoire courant de mon utilisateur ``~/UpdateCloudflareIp.sh``.


Pour en savoir plus sur la requête de mise à jour, vous pouvez [consulter la documentation](https://api.cloudflare.com/#dns-records-for-a-zone-update-dns-record).

1. On définit toutes les variables utiles pour la requête.
	Voici les valeurs à remplacer :
	- ``VOTRE_EMAIL`` par l'email de votre compte Cloudflare.
	- ``VOTRE_TOKEN`` par le token récupéré plus haut.
	- ``ZONE_DNS`` par la zone DNS de votre nom de domaine.
	- ``DNS_RECORD_ID`` par l'identifiant de l'entrée DNS.

	Options supplémentaires :
	- ``DNS_RECORD_TYPE`` change le type de l'entrée DNS.
	- ``DNS_RECORD_TTL`` change le TTL de l'entrée DNS. La valeur ``1`` veut dire automatique.
	- ``TEMP_FILE`` change le nom du fichier temporaire pour stocker l'ancienne IP.
	- ``CLOUDFLARE_PROXY`` permet d'activer ou non les services de Cloudflare.
		> Si activé (``true``), uniquement les ports 80 et 443 seront ouverts car Cloudflare renvoie une IP d'un proxy où les différents services de Cloudflare font effet. Votre IP est par conséquent caché.
		> Si désactivé (``false``), tous les ports sont accessibles. Néanmoins les services de Cloudflare ne sont plus disponibles et votre IP n'est plus caché par Cloudflare.

2. Le script récupère l'adresse IP publique de votre réseau depuis le site [ifconfig.co](https://ifconfig.co).
3. Il charge le fichier s'il existe de l'ancienne IP, sinon il définit que l'ancienne IP est une IP qui n'existe pas.
5. Si l'ancienne IP est différente de la nouvelle, le script appelle alors l'API de Cloudflare et met à jour l'IP. Le script affiche un message si tout c'est bien passé (ou non) et il enregistre la nouvelle IP dans le fichier temporaire.
6. Sinon le script affiche que l'IP est déjà à jour.

Voici le script :

```bash
#!/bin/bash

EMAIL='VOTRE_EMAIL'
TOKEN='VOTRE_TOKEN'

ZONE_ID='ZONE_DNS'
DNS_RECORD_ID='DNS_RECORD_ID'
DNS_RECORD_HOST='VOTRE_NOM_DE_DOMAINE'
DNS_RECORD_TYPE='A'
DNS_RECORD_TTL=1

CLOUDFLARE_PROXY="false"
TEMP_FILE="IP.tmp"


NEW_IP=$(curl -s 'http://v4.ifconfig.co')
OLD_IP=""

if [ -f "$TEMP_FILE" ]; then
	OLD_IP=$(head -n 1 $TEMP_FILE)
else
	OLD_IP="256.256.256.256"
fi

if [ "$NEW_IP" != "$OLD_IP" ]; then
	echo "$NEW_IP" > "$TEMP_FILE"
	RES=$(curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records/$DNS_RECORD_ID" -H "X-Auth-Email: $EMAIL" -H "X-Auth-Key: $TOKEN" -H 'Content-Type: application/json' --data "{\"type\":\"$DNS_RECORD_TYPE\",\"name\":\"$DNS_RECORD_HOST\",\"content\":\"$NEW_IP\",\"ttl\":$DNS_RECORD_TTL,\"proxied\":$CLOUDFLARE_PROXY}")
	if [[ "$RES" == *"\"success\":false"* ]]; then
		echo "Une erreur c'est produite lors de la mise à jour de l'adresse IP";
		exit 2;
	fi
	if [[ "$RES" == *"\"success\":true"* ]]; then
		echo "La mise à jour de l'adresse IP est un succès !";
		exit 0;
	fi
else
	echo "L'adresse IP est déjà à jour."
	exit 1;
fi
```

Une fois le fichier créé, il faut le rendre exécutable :
```
$ chmod +x ~/UpdateCloudflareIp.sh
```

> Pour vérifier que votre configuration fonctionne, exécutez le script et vérifiez que votre IP est bien à jour sur Cloudflare.
> Le script devrez vous afficher ``La mise à jour de l'adresse IP est un succès !``.

✔️ Script configuré et fini, passons à son lancement automatique.

### Lancement régulier du script
 éviter de rendre le site offline plusieurs temps en cas de changement d'IP, nous pouvons mettre un temps de rafraichissement court comme 5 minutes mais celui-ci entrainera un grand nombre de requêtes vers le site [ifconfig.co](https://ifconfig.co). Dans mon exemple, un temps de rafraichissement de 30 minutes est suffisant.

Pour lancer régulièrement le script, nous allons utiliser ``cron``.
Pour modifier son fichier de configuration, il faut utiliser ``crontab``.

```bash
$ sudo crontab -e
```

Utiliser ``sudo`` permet de s'assurer que le fichier sera exécuté mais cela peut poser des problèmes de sécurité (comme toute utilisation de ``sudo``). Celui-ci peut être exécuté en utilisateur mais la session de l'utilisateur devra être ouverte afin que le script soit lancé. S'il s'agit de la première fois que vous utilisez ``crontab``, celui-ci peut vous demander quel éditeur utiliser. Perso, j'aime bien ``nano`` du fait qu'il est simple à utiliser. Taper 2 pour ``nano``.

```
Select an editor.  To change later, run 'select-editor'.
  1. /bin/ed
  2. /bin/nano        <---- easiest
  3. /usr/bin/vim.basic
  4. /usr/bin/vim.tiny

Choose 1-4 [2]:
```

Une fois l'éditeur de fichier ouvert, ajouter dessous la dernière ligne du fichier la ligne suivante :

```
*/30 * * * * /home/nathan/UpdateCloudflareIp.sh
```

La première partie permet de définir à ``cron`` quand le script doit être exécuté. Ici, il sera exécuté toute les 30 minutes.
> Pour générer votre propre expression ``cron``, aller sur [ce site](https://crontab.guru/#30_*_*_*_*). Vous pouvez trouver une liste d'expression toute prête sur [cette page](https://crontab.guru/examples.html).

La deuxième partie correspond au chemin absolu du script.

✔️ Et voilà, maintenant le script sera appelé toutes les 30 minutes et il mettra à jour notre IP sur Cloudflare si elle a changé !