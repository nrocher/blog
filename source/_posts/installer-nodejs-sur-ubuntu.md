---
title: Installer Node.js sur Ubuntu
author: Nathan Rocher
tags: ["SysAdmin", "Linux", "Node.js", "npm"]
summary: Aujourd'hui, nous allons installer la dernière version ou la version LTS de Node.js. 
date: 2019-09-14 22:05:00
---

![Logo de Node.js](nodejs.png)

### Introduction
**Node.js** est un environnement d'exécution Javascript tournant sur le moteur V8 de Chrome.
C'est-à-dire qu'il permet d'exécuter des fichiers `.js`. Il est **multiplateforme** et est **open-source**.
Node.js est très complet. Il inclut par défaut de nombreuses bibliothèques permettant par exemple d'écrire des fichiers, ou établir des connexions réseaux.

Un avantage de Node.js, c'est que l'on peut copier des fichiers `.js` sur une autre machine avec une configuration totalement différrente et notre code fonctionnera toujours.

Node.js dispose de son propre gestionnaire de paquets `npm`. Il permet d'ajouter des paquets simplement, vérifier s'il existe des failles de sécurité dans les paquets qui sont ajoutés à votre projet, ou de vérifier si une nouvelle version d'un paquet existe.

Lors de l'installation de Node.js, `npm` sera automatiquement installé.

[Node.js](https://nodejs.org/fr/) : Site officel de Node.js
[npm](https://npmjs.com) : Site officel de npm

Ce tutoriel vous montrera comment installer la dernière version de Node.js ou la dernière version LTS.
Vous pouvez trouver toutes les versions pour Ubuntu [sur le git de NodeSource](https://github.com/nodesource/distributions/blob/master/README.md).

### Installer Node.js 
Il existe deux types de version de Node.js :
1. Current version : Il s'agit d'une version stable contenant les dernières nouveautés.
2. Version LTS (Long Time Support) : Il s'agit d'une version stable à utiliser en **production**. 

À l'heure où j'écris cet article la current version est *12.10.0* et la version LTS est *10.16.3*.

Vous pouvez trouver la documentation de la version *12.10.0* [ici](https://nodejs.org/dist/latest-v12.x/docs/api/).
Et la documentation de la version *10.16.3* [ici](https://nodejs.org/dist/latest-v10.x/docs/api/).

>⚠️ Attention, vous devez avoir au minimum Ubuntu 16.04.
>Les versions antérieures ne supportent pas les versions de Node.js > 10.

Pour pouvoir installer Node.js, nous allons avoir besoin de `curl`.
Si vous ne l'avez pas : `sudo apt install curl`.

Pour installer la Current version :
```bash
$ curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
$ sudo apt install -y nodejs
```

Pour installer la version LTS :
```bash
$ curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
$ sudo apt install -y nodejs
```

### Vérifier l'installation

Lors de la réalisation du tutoriel j'ai installé la Current version.

Dans un bash, taper :
```bash
$ node -v
```
Node.js devrait alors afficher :
```
v12.10.0
```

Vous pouvez aussi essayer npm :
```bash
$ npm -v
```
npm devrait alors afficher :
```
6.10.3
```

✔️ Vous avez correctement installé Node.js et npm !