---
title: Liste de liens utiles
date: 2019-09-11 22:23:32
---

### Emoji
[Liste des emojis](/emoji) : Liste des emojis
[Twemoji](https://twemoji.twitter.com/) : Emoji open-source de twitter (utilisé ici)

### Design
[hexo-theme-Anatole](https://github.com/Ben02/hexo-theme-Anatole) : Thème original que j'ai modifié

### Code
[Hexo](https://hexo.io/) : Site officiel du framework de ce blog
[web.dev Measure](https://web.dev/measure) : Analyse les performances techniques d'un site web