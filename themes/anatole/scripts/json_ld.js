/**
 * Builds JSON-LD structured data for current page according to its type (page or post).
 *
 * @returns {string} - JSON-LD structured data
 */
function jsonLd() {
	const page = this.page;
	const config = this.config;
	const theme = this.theme;
	const authorEmail = "3007nat@gmail.com";
	const authorImage = "http://nrocher.fr/images/logo@2x.jpg";
	const links = [];

	const author = {
	  '@type': 'Person',
	  name: "Nathan Rocher",
	  sameAs: links
	};
	// Google does not accept `Person` as item type for the publisher property
	const publisher = Object.assign({}, author, {'@type': 'Organization'});
	let schema = {};
  
	if (authorImage) {
	  author.image = authorImage;
	  publisher.image = authorImage;
	  publisher.logo = {
		'@type': 'ImageObject',
		url: authorImage
	  };
	}
  
	if (this.is_post()) {
	  let images = [];
	  schema = {
		'@context': 'http://schema.org',
		'@type': 'BlogPosting',
		author: author,
		dateCreated: page.date.format(),
		datePublished: page.date.format(),
		description: this.strip_html(page.summary),
		headline: page.title,
		image: images,
		mainEntityOfPage: {
		  '@type': 'WebPage',
		  '@id': this.url_for(page.permalink)
		},
		publisher,
		url: this.url_for(page.permalink)
	  };
  
	  if (page.tags && page.tags.length > 0) {
		schema.keywords = page.tags.map((tag) => tag.name).join(', ');
	  }
  
	  if (page.photos && page.photos.length > 0) {
		images = images.concat(page.photos);
	  }
  
	  if (page.coverImage) {
		images.unshift(page.coverImage);
	  }
  
	  if (page.thumbnailImage || page.coverImage) {
		images.unshift(page.thumbnailImage);
		schema.thumbnailUrl = page.thumbnailImage || page.coverImage;
	  }
  
	  schema.image = images;
	}
	else if (this.is_page() || this.is_home()) {
	  schema = {
		'@context': 'http://schema.org',
		'@type': 'Website',
		'@id': config.url,
		author: author,
		name: config.title,
		description: config.description,
		url: config.url
	  };
  
	  if (config.keywords && config.keywords.length) {
		schema.keywords = config.keywords.join(', ');
	  }
	}

    return JSON.stringify(schema);
  }
  
  hexo.extend.helper.register('json_ld', jsonLd);