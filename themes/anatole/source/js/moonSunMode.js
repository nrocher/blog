
function moonSunMode(load) {
	if(load === true){
		const currentTheme = localStorage.getItem('theme') ? localStorage.getItem('theme') : null;

		if (currentTheme) {
			document.documentElement.setAttribute('data-theme', currentTheme);

			if (currentTheme === 'dark') {
				setDark()
			} else {
				setLight()
			}
		} else {
			if(window.matchMedia("(prefers-color-scheme: dark)").matches) {
				setDark()
			} else {
				setLight()
			}
		}
	} else {
		if (document.documentElement.getAttribute("data-theme") === "light") {
			setDark()
		} else {
			setLight()
		}
	}
}

function setDark(){
	document.documentElement.setAttribute('data-theme', 'dark');
	document.getElementById("moonSunMode").innerText = "☀️ Jour"
	localStorage.setItem('theme', 'dark');
}

function setLight(){
	document.documentElement.setAttribute('data-theme', 'light');
	document.getElementById("moonSunMode").innerText = "🌑 Nuit"
	localStorage.setItem('theme', 'light');
}